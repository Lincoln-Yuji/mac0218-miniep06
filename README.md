# Resolução Mini EP06

```
(Parte 1)
sudo docker container run -p 8080:80 --name miniep_container caddy

-- Já existe um Caddyfile dentro do container com root * usr/share/caddy
sudo docker cp index.html miniep_container:/usr/share/caddy
sudo docker cp about.html miniep_container:/usr/share/caddy

-- Testando a conexão (pode ser num browser)
curl http://localhost:8080

(Parte 2)
sudo docker container run -p 8080:80 -v ${PWD}:/usr/share/caddy --name miniep_container caddy

-- Usando volumes não precisamos mais copiar o index.html e o about.html para o docker
-- visto que criamos um link do diretório atual com os htmls do nosso sistema de arquivos com o container

-- Testando a conexão
curl http://localhost:8080

-- Modifica o h1 do index.html para "Modificado" no nosso filesystem fora do container

--Testando a conexão de novo
curl http://localhost:8080

```
